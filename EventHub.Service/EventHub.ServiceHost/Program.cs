﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EventHub.Service;

namespace EventHub.ServiceHost
{
    class Program
    {
        static void Main(string[] args)
        {
            System.ServiceModel.ServiceHost host=new System.ServiceModel.ServiceHost(typeof(EventPublishService));
            host.Open();
            Console.WriteLine("Service started...");

            Console.WriteLine("Press enter to stop the service");
            
            Console.ReadLine();
            host.Close();
        }
    }
}
