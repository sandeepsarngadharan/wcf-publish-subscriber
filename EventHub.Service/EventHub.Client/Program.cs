﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Text;
using System.Threading.Tasks;
using EventHub.Service;

namespace EventHub.Client
{
    public class Program
    {
        static void Main(string[] args)
        {
            var callback = new EventNotificationHandler();
            var client = new Subcriber(callback, new NetTcpBinding(), new EndpointAddress("net.tcp://localhost:2222"));
            var proxy = client.ChannelFactory.CreateChannel();
            proxy.Subscribe();
            Console.WriteLine("client subscribed..");
            Console.WriteLine("Press enter to close the client..");
            Console.ReadLine();
            client.Close();
            
        }
    }

    public class EventNotificationHandler : IEventNotificationContract
    {
        public void OnEventPublished(EventData data)
        {
            Console.WriteLine($"Server published {data.Name}, {data.Type} on {data.Date:U}");
        }
    }

    public class Subcriber : DuplexClientBase<IEventPublishService>
    {
        public Subcriber(object callbackInstance, Binding binding, EndpointAddress remoteAddress)
            : base(callbackInstance, binding, remoteAddress) { }
    }

}
