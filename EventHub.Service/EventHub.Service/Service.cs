﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace EventHub.Service
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single)]
    public class EventPublishService : IEventPublishService
    {
        public delegate void PublishEventHandler(EventData e);
        public static event PublishEventHandler OnPublishEvent;
        public List<IEventNotificationContract> _subscribers=new List<IEventNotificationContract>();

        private IEventNotificationContract _subscriber = null;
        private PublishEventHandler _subscriberHandler = null;
        private bool IsStarted { get; set; }

        public void Subscribe()
        {
            _subscriber = OperationContext.Current.GetCallbackChannel<IEventNotificationContract>();
            _subscribers.Add(_subscriber);
            Console.WriteLine("Client registered");
           
        }

        public void Unsubscribe()
        {
            OnPublishEvent -= _subscriberHandler;
        }

        public void PublishEventData(EventData data)
        {
            Console.WriteLine("Raising event now");
            RaisePublishEvent(data);
        }

        private void RaisePublishEvent(EventData e)
        {
            foreach (var subscriber in _subscribers)
            {
                subscriber.OnEventPublished(e);
            }

        }
    }
}
