﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using EventHub.Service;

namespace EventHub.Service
{
    [ServiceContract(SessionMode = SessionMode.Required, CallbackContract = typeof
            (IEventNotificationContract))]
    public interface IEventPublishService
    {
        [OperationContract(IsInitiating = true)]
        void Subscribe();
        [OperationContract(IsInitiating = true)]
        void Unsubscribe();
        [OperationContract(IsInitiating = true)]
        void PublishEventData(EventData data);
    }

    public interface IEventNotificationContract
    {
        [OperationContract(IsOneWay = true)]
        void OnEventPublished(EventData data);
    }

}



