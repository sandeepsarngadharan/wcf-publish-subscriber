﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Text;
using System.Threading.Tasks;
using EventHub.Service;

namespace EventHub.Trigger
{
    class Program
    {
        static void Main(string[] args)
        {
            var callback = new EventNotificationHandler();
            var client = new Subcriber(callback, new NetTcpBinding(), new EndpointAddress("net.tcp://localhost:2222"));
            var proxy = client.ChannelFactory.CreateChannel();
            while (true)
            {
                Console.WriteLine("sending new event");
                proxy.PublishEventData(new EventData
                {
                    Date = DateTime.Now,
                    Name = "Event Data",
                    Type = "Event"
                });
                System.Threading.Thread.Sleep(TimeSpan.FromSeconds(10));
            }
        }
    }

    public class EventNotificationHandler : IEventNotificationContract
    {
        public void OnEventPublished(EventData data)
        {
            Console.WriteLine($"Server published {data.Name}, {data.Type} on {data.Date:U}");
        }
    }

    public class Subcriber : DuplexClientBase<IEventPublishService>
    {
        public Subcriber(object callbackInstance, Binding binding, EndpointAddress remoteAddress)
            : base(callbackInstance, binding, remoteAddress) { }
    }
}
